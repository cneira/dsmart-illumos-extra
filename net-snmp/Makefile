#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License, Version 1.0 only
# (the "License").  You may not use this file except in compliance
# with the License.
#
# You can obtain a copy of the license at COPYING
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at COPYING.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
# Copyright 2015 Argo Technologie SA.
#

VER =	net-snmp-5.4.1

include ../Makefile.defs

PARALLEL = -j1

BUILD32 =	yes
BUILD64 =	yes

CFLAGS += -std=gnu99
CPPFLAGS += -DFALSE_SHARING_ALIGN=64

ifeq ($(STRAP),strap)
	LDFLAGS += -L$(DESTDIR)/usr/lib -R$(DESTDIR)/usr/lib
	LDFLAGS.64 += -L$(DESTDIR)/usr/lib/amd64 -R$(DESTDIR)/usr/lib/amd64
endif

AUTOCONF_OPTS += --with-default-snmp-version=3
AUTOCONF_OPTS += --with-sys-contact="root@localhost"
AUTOCONF_OPTS += --with-sys-location=Unknown
AUTOCONF_OPTS += --with-logfile=/var/log/snmpd.log
AUTOCONF_OPTS += --with-persistent-directory=/var/net-snmp

AUTOCONF_OPTS += --with-mibdirs=/etc/net-snmp/snmp/mibs
AUTOCONF_OPTS += --datadir=/etc/net-snmp
AUTOCONF_OPTS += --enable-agentx-dom-sock-only
AUTOCONF_OPTS += --enable-ucd-snmp-compatibility
AUTOCONF_OPTS += --enable-ipv6
AUTOCONF_OPTS += --enable-mfd-rewrites
AUTOCONF_OPTS += --with-pkcs
AUTOCONF_OPTS += --with-transports="UDP TCP UDPIPv6 TCPIPv6"

AUTOCONF_OPTS += --without-python-modules

# Which MIB modules do we want to build
MIB_MODULES = host disman/event-mib ucd-snmp/diskio udp-mib tcp-mib if-mib
AUTOCONF_OPTS += --with-mib-modules="$(MIB_MODULES)"

#PERL_ARGS = DESTDIR=$(DESTDIR) INSTALLDIRS=vendor

AUTOCONF_OPTS.32 += \
	--disable-embedded-perl \
	--without-perl-modules

AUTOCONF_OPTS.64 += \
	--disable-embedded-perl \
	--without-perl-modules
AUTOCONF_OPTS.64 += \
	--libdir=/usr/lib/amd64

AUTOCONF_ENV += PKG_CONFIG_PATH="$(DESTDIR)/usr/lib/pkgconfig"
AUTOCONF_ENV.64 += PKG_CONFIG_PATH="$(DESTDIR)/usr/lib/amd64/pkgconfig"

PATCHES +=	patches/*

include ../Makefile.targ
include ../Makefile.targ.autoconf
